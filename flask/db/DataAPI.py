from db.DataBase import connection
from pymysql import escape_string as clean
from passlib.hash import sha256_crypt

def add_user(name,username,email,password):
	c_name = clean(name)
	c_username = clean(username)
	c_email = clean(email)
	c_password = sha256_crypt.encrypt(clean(password))

	#create connection
	conn,db = connection()

	#execute query

	query = db.execute("INSERT INTO users (name,username,email,password,admin) VALUES (%s,%s,%s,%s,0)",(c_name, c_username, c_email, c_password))

	#save change
	conn.commit()

	#close connection
	db.close()

def check_user(username):
	c_username = clean(username)
	conn,db = connection()

	query = db.execute('SELECT * FROM users WHERE username = %s',[c_username])

	#fetch data
	data = db.fetchone()
	return query,data

def Add_article(title, user, content):
	c_title = clean(title)
	c_content = clean(content)

	conn,db = connection()

	query = db.execute("INSERT INTO articles_base (title, author, content, approve) VALUES (%s, %s, %s, 0)",(
		c_title, user, c_content))
	conn.commit()
	db.close()

def show_article():
	conn,db = connection()
	query = db.execute('SELECT * FROM articles_base WHERE approve=0')
	data = db.fetchall()
	return query,data


def get_by_id(id):
	c_id = clean(id)
	conn,db = connection()
	query = db.execute('SELECT * FROM articles_base WHERE id=%s',[c_id])
	data = db.fetchone()
	return query,data


def update(id, title, content):
	c_id = clean(id)
	c_title = clean(title)
	c_content = clean(content)
	conn,db = connection()

	query = db.execute('UPDATE articles_base SET title=%s, content = %s WHERE id = %s',(c_title,c_content,c_id))

	conn.commit()
	db.close()


def delete(id, username):
	c_id = clean(id)
	c_username = clean(username)
	conn,db = connection()

	query = db.execute('DELETE FROM articles_base WHERE id = %s AND author = %s',(c_id, c_username))

	conn.commit()
	db.close()