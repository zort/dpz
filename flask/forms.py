from wtforms import Form, StringField, PasswordField, TextAreaField, validators

class RegisterForm(Form):
	name = StringField('Nazwa',[validators.Length(min=4, max=20)])
	username = StringField('Login',[validators.Length(min=4, max=15)])
	email = StringField('E-mail',[validators.Length(min=6, max=50)])
	password = PasswordField('Nowe hasło', [
		validators.DataRequired(),
		validators.EqualTo('confirm', message='Hasła muszą się zgadzać!')
	])
	confirm = PasswordField('Powtórz hasło')

class Add_article_form(Form):
	title = StringField('Tytuł',[validators.Length(min=3,max=25)])
	content = TextAreaField('Treść',[validators.Length(min=1)])
		