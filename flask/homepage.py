from __future__ import print_function # In python 2.7
from flask import Flask, flash, render_template,send_from_directory, request, session, url_for, redirect, logging, send_file
import datetime
import shutil
from forms import RegisterForm, Add_article_form
from db.DataBase import connection
from db.DataAPI import add_user, check_user, Add_article, show_article, get_by_id, update, delete
import os
from requests.auth import HTTPDigestAuth
from requests.auth import HTTPBasicAuth
import sys
from flask import stream_with_context, Response
import os.path, time
from passlib.hash import sha256_crypt
from flask_bootstrap import Bootstrap


#notesDB = notesDB()

app = Flask(__name__)

@app.route('/')
def home():
	projectName = 'Flaskapp'
	return render_template("home.html", projectName=projectName)

@app.route('/about')
def about():
	projectName = 'aboutpage'
	return render_template("about.html", projectName=about)

@app.route('/notes')
def notes():
	query,data=show_article()

	if query > 0:
		notes = data
		return render_template("notes.html", notes=notes)
	else:
		msg = "No data yet"
		return render_template("notes.html", msg=msg)

@app.route('/create',methods=['GET','POST'])
def Article_create():
	form = Add_article_form(request.form)
	if request.method=="POST" and form.validate():
		title = form.title.data
		content = form.content.data
		user = session['username']

		Add_article(title, user, content)
		flash("Wpis zapisany","success")
		return redirect(url_for('notes'))
	return render_template('create_article.html', form=form)


@app.route('/edit/<string:id>', methods=['GET','POST'])
def note_edit(id):
	form = Add_article_form(request.form)
	query, data = get_by_id(id)
	form.title.data = data['title']
	form.content.data = data['content']

	if request.method=="POST" and form.validate():
		title = request.form['title']
		content = request.form['content']
		update(id,title,content)
		flash("Notatka zapisana","success")
		return render_template('create_article.html',form=form)
		
	return render_template('create_article.html',form=form)




@app.route('/notes/<string:id>')
def article_page(id):
	query, data = get_by_id(id)
	if query > 0:
		artd = data
		return render_template("article_page.html", artd=artd)
	else:
		msg = "Brak notatki z ID "+str(id)+ "."
		return render_template("article_page.html", msg=msg)


@app.route('/delete/<string:id>')
def article_delete(id):
	query,data = get_by_id(id)
	username = session['username']
	if data['author'] in username:
		delete(id,username)
		flash("Notatka skasowana","success")
		return redirect(url_for('notes'))
	else:
		flash("Nie masz uprawień by wykonać tę akcję","info")
		return redirect(url_for('notes'))




@app.route('/download/')
def file_downloads():
	date = datetime.datetime.now()
	x = date
	source_path = "/media/hikvision"
	check_dir = os.path.isdir(source_path)
	if check_dir == True:
		exists = "Katalog istnieje"
		files = os.listdir(source_path)

		#mix2 = ("created: %s" % time.ctime(os.path.getctime("/media/hikvision/2018123006.mp4")))
	else: 
		exists = "Katalog nie istnieje"

	try:
		return render_template('download.html', files=files, source_path=source_path)
	except Exception as e:
		return str(e)
 

@app.route('/download/<string:video>')
def download(video):
	path = "/media/hikvision/"
	return send_file(path+video)


@app.route('/register', methods=['GET','POST'])
def register():
	# try:
	# 	conn,db = connection()
	# 	msg = "OK"
	# except:
	# 	msg = connection()

	form = RegisterForm(request.form)
	if request.method == "POST" and form.validate():
		#get data from the form
		name = form.name.data
		username = form.name.data
		email = form.name.data
		password = form.name.data
		#save in db
		add_user(name,username,email,password)
		#session
		session['username'] = username
		session['logged_in'] = True

		flash ("Witaj {}".format(username),"success")
		return redirect(url_for("home"))
	#data = notesDB()
	return render_template("register.html", form=form)

@app.route('/login', methods=['GET','POST'])
def login():
	if request.method == "POST":
		username = request.form['username']
		password = request.form['password']
		query,data = check_user(username)
		if query>0:
			_password = data['password']
			if sha256_crypt.verify(password,_password):
				#session
				session['username'] = username
				session['logged_in'] = True
				flash ("Welcome {}".format(username),"success")
				return redirect(url_for('home'))
			else:
				error = "Login lub hasło niepoprawne"
				return render_template("login.html", error=error)
		else:
			error = "Login lub hasło niepoprawne"
			return render_template("login.html", error=error)
	return render_template("login.html")


@app.route('/logout', methods=['GET','POST'])
def logout():
	session.clear()
	flash("Zostałeś poprawnie wylogowany","warning")
	return redirect(url_for("login"))

@app.route('/test')
def test():
	
	projectName = 'Testpage on homepage py'
	return render_template("test.html", projectName=projectName, plic=plic)



if __name__=="__main__":
	#app.secret_key = "943j3c#G##b#B#34533g"
	app.secret_key = os.urandom(24)
	#app.run()
	app.run(debug=True, host='10.8.0.6')

